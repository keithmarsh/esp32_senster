#pragma once

#include "Arduino.h"

class AX12
{
public:
  AX12(HardwareSerial &serial, int id);
  bool init();
  int id();
  bool isMoving(int &isMoving);
  bool getPosition(int &position);
  bool setSpeed(int speed);
  bool setPosition(int position);
  bool setAddr(int addr, int val);
  bool setAddr2(int addr, int val);
  bool getAddr(int Addr, int &val);
  bool getAddr2(int Addr, int &val);
protected:
  int mID;
  HardwareSerial &mSerial;
  unsigned char checksum(unsigned char *buffer);
  void send(int len);
  bool recv(int len);
  void printError(int stat);
  unsigned char SEND_BUFFER[20];
  unsigned char RECV_BUFFER[20];
};

