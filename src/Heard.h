#pragma once
struct Heard {
  int loudness;    // sample amplitude
  float peak;      // certainty of correlation
  float direction; // -1.0 tp 1.0
};
