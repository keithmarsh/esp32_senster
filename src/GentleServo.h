#ifndef GENTLE_SERVO_H
#define GENTLE_SERVO_H

#include "AX12.h"

class GentleServo {
  
public:
  /*
   * ID = servo id
   * maxSpeed = degrees / second
   * accel = degrees / second / second
   */
  GentleServo(int ID, int maxSpeed, int accel, HardwareSerial &serial1);
  static void start(long baud);
  void setTarget(int pos);
  int getCurrentPosition();
  bool isMoving();
  void tick(int mSec);
  bool setIDBeCareful(int ID);
 
protected:
  int mMaxSpeed;
  long mAccel;
  long mAccelDistance;
  int mCurrSpeed;
  int mGoalSpeed;
  long mMSecMoving;
  long mMSecToTarget;
  int mStartPos;
  int mGoalPos;
  AX12 mAX12;
  static const int sTopSpeed = 300;
  static const int sStartSpeed = 10;
};

#endif

