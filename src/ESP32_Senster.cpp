#include <Arduino.h>
#include "GentleServo.h"
#include "ListenI2S.h"
#include "Heard.h"

HardwareSerial Serial1(2); // rx:9 & tx:10
static const int NUM_SERVOS = 3;
GentleServo *mServos[NUM_SERVOS];
int mMode = 0;
int mServoIx = 0;
unsigned long tickMillis = millis();
unsigned long nextEvent = 0;
static const long baudrate = 1000000;
ListenI2S *mListen;

bool allNotMoving();

void setup() {
  Serial.begin(115200);
  while (!Serial) ;
  Serial1.begin(baudrate);
  Serial1.setTimeout(1L);
  while (!Serial1) ;
  GentleServo::start(baudrate);
  delay(1000);
  mServos[0] = new GentleServo ( 0, 100, 50, Serial1 ); // 240
  mServos[1] = new GentleServo ( 1, 100, 50, Serial1 );
  mServos[2] = new GentleServo ( 2, 100, 50, Serial1 );
  mListen = new ListenI2S(26, 21, 4, 8192, 60);

  mServos[0]->setTarget(512);
  mServos[1]->setTarget(512);
  mServos[2]->setTarget(512);
}

void loop() {
  unsigned long currMillis = millis();
  if (currMillis - tickMillis > 10)
  {
    if (true) { //allNotMoving()) {
      Heard heard = mListen->listen();
      //int direction = mListen->listen();
      if (heard.peak >= 0.9f && heard.loudness > 5) {
        int target = 512 + (heard.direction * 512);
        Serial.print("target ");
        Serial.println(target);
        mServos[0]->setTarget(target);
        if (heard.loudness > 500) {
          mServos[1]->setTarget(200);
          mServos[2]->setTarget(200);
        } else {
          mServos[1]->setTarget(512);
          mServos[2]->setTarget(512);
        }
      }
    }
    unsigned long deltaMillis = currMillis - tickMillis;
    tickMillis = currMillis;
    for (int ix = 0; ix < NUM_SERVOS; ix++) {
      mServos[ix]->tick(deltaMillis);
    }
  }
  delay(1);
}

bool allNotMoving() {
  return ! (mServos[0]->isMoving() || mServos[1]->isMoving() || mServos[2]->isMoving());
}
