#include <Arduino.h>
#include "AX12.h"

#define START_BYTE 0xFF

#define INST_PING 0x01
#define INST_READ 0x02
#define INST_WRITE 0x03
#define INST_REG_WRITE 0x04
#define INST_ACTION 0x05
#define INST_RESET 0x06
#define INST_DIGITAL_RESET 0x07
#define INST_SYSTEM_READ 0x0C
#define INST_SYSTEM_WRITE 0x0D
#define INST_SYNC_WRITE 0x83
#define INST_SYNC_REG_WRITE 0x84

#define P_ID (3)
#define P_RETURN_DELAY_TIME (5)
#define P_STATUS_RETURN_LEVEL (16)
#define P_GOAL_POSITION_L (30)
#define P_GOAL_POSITION_H (31)
#define P_MOVING_SPEED_L (32)
#define P_MOVING_SPEED_H (33)
#define P_PRESENT_POSITION_L (36)
#define P_PRESENT_POSITION_H (37)
#define P_MOVING (46)

#define STATUS_INPUT_VOLTAGE_ERROR (0x01)
#define STATUS_ANGLE_LIMIT_ERROR   (0x02)
#define STATUS_OVERHEATING_ERROR   (0x04)
#define STATUS_RANGE_ERROR         (0x08)
#define STATUS_CHECKSUM_ERROR      (0x10)
#define STATUS_OVERLOAD_ERROR      (0x20)
#define STATUS_INSTRUCTION_ERROR   (0x40)

#define RECV_IX_ERR   (4)
#define RECV_IX_DATA1 (5)
#define RECV_IX_DATA2 (6)

#define DIRECTION_PIN (7)
#define DIRECTION_WR (HIGH)
#define DIRECTION_RD (LOW)

#define LOG (0)

AX12::AX12(HardwareSerial &serial, int id)
  : mID ( id )
  , mSerial ( serial )
{
}

bool AX12::init() {
  pinMode(DIRECTION_PIN, OUTPUT);
  digitalWrite(DIRECTION_PIN, DIRECTION_RD);

  Serial.print(mID);
  Serial.print(" ping ");
  int sendIx = 0;
  SEND_BUFFER[sendIx++] = START_BYTE;
  SEND_BUFFER[sendIx++] = START_BYTE;
  SEND_BUFFER[sendIx++] = mID;
  SEND_BUFFER[sendIx++] = 0; // LEN goes here
  SEND_BUFFER[sendIx++] = INST_PING;
  send(sendIx);

  bool isOK = recv(6);
  Serial.println("done");
  if (isOK) {
    setAddr(P_STATUS_RETURN_LEVEL, 2);
    isOK = setAddr(P_RETURN_DELAY_TIME, 150);
  }
  return isOK;
}

int AX12::id() {
  return mID;
}

bool AX12::setAddr(int addr, int val)
{
  Serial.print(mID);
  Serial.print(" setAddr(");
  Serial.print(addr);
  Serial.print(", ");
  Serial.print(val);
  Serial.print(") ");

  int sendIx = 4;
  SEND_BUFFER[sendIx++] = INST_WRITE;
  SEND_BUFFER[sendIx++] = addr;
  SEND_BUFFER[sendIx++] = val & 0xff;
  send(sendIx);
  bool isOK = recv(6);
  Serial.println(" done ");
  return isOK;
}

bool AX12::setAddr2(int addr, int val)
{
#if LOG
  Serial.print(mID);
  Serial.print(" setAddr2(");
  Serial.print(addr);
  Serial.print(", ");
  Serial.print(val);
  Serial.print(") ");
#endif
  int sendIx = 4;
  SEND_BUFFER[sendIx++] = INST_WRITE;
  SEND_BUFFER[sendIx++] = addr;
  SEND_BUFFER[sendIx++] = val & 0xff;
  SEND_BUFFER[sendIx++] = (val & 0xff00) >> 8;
  send(sendIx);
  bool isOK = recv(6);
#if LOG
  Serial.println(" done ");
#endif
  return isOK;
}

bool AX12::getAddr(int addr, int &val) {
#if LOG
  Serial.print(mID);
  Serial.print(" getAddr(");
  Serial.print(addr);
  Serial.print(") ");
#endif
  int sendIx = 4;
  SEND_BUFFER[sendIx++] = INST_READ;
  SEND_BUFFER[sendIx++] = addr;
  SEND_BUFFER[sendIx++] = 1;
  send(sendIx);
  bool isOK = recv(7);
  if (isOK) {
    val = RECV_BUFFER[RECV_IX_DATA1];
#if LOG
    Serial.print(" done ");
    Serial.println(val);
#endif
  }
  return isOK;
}

bool AX12::getAddr2(int addr, int &val) {
#if LOG
  Serial.print(mID);
  Serial.print(" getAddr2(");
  Serial.print(addr);
  Serial.print(") ");
#endif
  int sendIx = 4;
  SEND_BUFFER[sendIx++] = INST_READ;
  SEND_BUFFER[sendIx++] = addr;
  SEND_BUFFER[sendIx++] = 2;
  send(sendIx);
  bool isOK = recv(8);
  if (isOK) {
    val = constrain(RECV_BUFFER[RECV_IX_DATA1] | (RECV_BUFFER[RECV_IX_DATA2] << 8), 0, 1024);
#if LOG
    Serial.print(" done ");
    Serial.println(val);
#endif
  }
  return isOK;

}

bool AX12::isMoving(int &isMoving) {
  return getAddr(P_MOVING, isMoving);
}

bool AX12::getPosition(int &position) {
  return getAddr2(P_PRESENT_POSITION_L, position);
}

bool AX12::setSpeed(int speed) {
  return setAddr2(P_MOVING_SPEED_L, speed);
}

bool AX12::setPosition(int position) {
  return setAddr2(P_GOAL_POSITION_L, position);
}

unsigned char AX12::checksum(unsigned char *buffer) {
  unsigned int ck = 0;
  for (int ix = 2; ix < buffer[3] + 3; ix++) {
    ck += buffer[ix];
  }
  return (~ck) & 0xff;
}

void AX12::send(int len) {
  //mSerial.flush();
  SEND_BUFFER[3] = len - 3;
  SEND_BUFFER[len] = checksum(SEND_BUFFER);

//  digitalWrite(DIRECTION_PIN, DIRECTION_RD);
//  int crap = 0;
//  while (mSerial.available()) {
//    mSerial.read();  crap++;
//  }
//  if (crap > 0) {
//    Serial.print(crap);
//    Serial.print("c ");
//  }
  digitalWrite(DIRECTION_PIN, DIRECTION_WR);

#ifndef ARDUINO_ARCH_ESP32
  int delayed = 0;
  while (mSerial.availableForWrite() < len & delayed < 10) {
    delay(1); delayed++;
  }
  if (delayed > 5) {
    Serial.print(delayed);
    Serial.print("wd ");
  }
  if (mSerial.availableForWrite() < len) {
    Serial.print(" no space! ");
    return;
  }
#endif
  for (int ix = 0; ix <= len; ix++) {
    int wrote = mSerial.write(SEND_BUFFER[ix]);
    if (wrote != 1) {
      Serial.print("zz ");
    }
  }
  digitalWrite(DIRECTION_PIN, DIRECTION_RD);
}

//
// Receive
//
bool AX12::recv(int len) {
  digitalWrite(DIRECTION_PIN, DIRECTION_RD);
#if 1
  int delayed = 0;
  while (!mSerial.available() && delayed < 50) {
    delay(1); delayed++;
  }
  if (delayed > 5) {
    Serial.print(delayed);
    Serial.print("rd ");
  }
#endif
#if 1
  int readBytes = mSerial.readBytes(RECV_BUFFER, len);
#else
  int readBytes = 0;
  int data = -1;
  while (readBytes < len && mSerial.available()) {
    RECV_BUFFER[readBytes] = data = mSerial.read();
    if (data >= 0) {
      readBytes++;
    } else {
      break;
    }
  }
#endif
#if LOG
  for (int ix = 0; ix < readBytes; ix++) {
    Serial.print(RECV_BUFFER[ix], HEX);
    Serial.print(" ");
  }
  if (readBytes != len) {
    Serial.print(readBytes);
    Serial.print("LEN");
    Serial.print(len);
    Serial.print(" ");
  } else {
    if (RECV_BUFFER[RECV_IX_ERR] != 0) {
      printError(RECV_BUFFER[RECV_IX_ERR]);
    }
  }
#endif
  int ck = checksum(RECV_BUFFER);
  return readBytes == len && RECV_BUFFER[RECV_IX_ERR] == 0 && ck == RECV_BUFFER[len-1];
}

void AX12::printError(int stat) {
  if ( stat & STATUS_INPUT_VOLTAGE_ERROR ) Serial.print("VOLTAGE ");
  if ( stat & STATUS_ANGLE_LIMIT_ERROR ) Serial.print("ANGLE ");
  if ( stat & STATUS_OVERHEATING_ERROR ) Serial.print("HEAT ");
  if ( stat & STATUS_RANGE_ERROR ) Serial.print("RANGE ");
  if ( stat & STATUS_CHECKSUM_ERROR ) Serial.print("CHECKSUM ");
  if ( stat & STATUS_OVERLOAD_ERROR ) Serial.print("OVERLOAD ");
  if ( stat & STATUS_INSTRUCTION_ERROR ) Serial.print("INSTRUCTION ");
  Serial.print("error\n");
}
