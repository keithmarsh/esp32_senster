#include <Arduino.h>
#include <math.h>
#include "ListenI2S.h"
#include "driver/i2s.h"

static const i2s_port_t i2s_num = I2S_NUM_0;

ListenI2S::ListenI2S(int bck, int ws, int dataIn, int i2sBuffLen, int maxDelay)
  : mI2SBufferLen ( i2sBuffLen )
  , mAudioLen ( i2sBuffLen >> 3 )
  , mMaxDelay ( min(maxDelay, mAudioLen) )
  , mXCorrLen ( mMaxDelay * 2 - 1 )

{
  i2s_config_t i2sConfig;
  i2sConfig.mode = (i2s_mode_t) ((int) I2S_MODE_MASTER | (int) I2S_MODE_RX);
  i2sConfig.sample_rate = 64000;
  i2sConfig.bits_per_sample = I2S_BITS_PER_SAMPLE_8BIT;
  i2sConfig.channel_format = I2S_CHANNEL_FMT_RIGHT_LEFT ;
  i2sConfig.communication_format = I2S_COMM_FORMAT_I2S_MSB;
  i2sConfig.intr_alloc_flags = 0;
  i2sConfig.dma_buf_count = 2;
  i2sConfig.dma_buf_len = 256;
  i2sConfig.use_apll = 0;
  i2s_pin_config_t pin_config;
  pin_config.bck_io_num   = bck;
  pin_config.ws_io_num    = ws;
  pin_config.data_out_num = 13;
  pin_config.data_in_num  = dataIn;
  /* esp_err_t status = */ i2s_driver_install(i2s_num, &i2sConfig, 0, nullptr);
  /* status = */ i2s_set_pin(i2s_num, &pin_config);
  /* status = */ i2s_set_clk(i2s_num, 96000, I2S_BITS_PER_SAMPLE_32BIT, I2S_CHANNEL_STEREO);
  mI2SBuffer = (uint8_t *) malloc(mI2SBufferLen);
  mI2SEOB = mI2SBuffer + mI2SBufferLen;
  mLeftAudio = (int32_t *) malloc(mAudioLen * sizeof(int32_t));
  mRightAudio = (int32_t *) malloc(mAudioLen * sizeof(int32_t));
  mXCorr = (double *) malloc(mMaxDelay * sizeof(double));
}

ListenI2S::~ListenI2S() {
  free(mI2SBuffer);
  free(mLeftAudio);
  free(mRightAudio);
  free(mXCorr);
}


Heard &ListenI2S::listen() {
  captureI2S();
  return crossCorrelate();
}

void ListenI2S::captureI2S()
{
  int bytesRead = i2s_read_bytes(i2s_num, (char *) mI2SBuffer, mI2SBufferLen, portMAX_DELAY );
  int32_t *buf = (int32_t *) mI2SBuffer;
  int32_t *left = mLeftAudio;
  int32_t *right = mRightAudio;
  while ( (unsigned char *) buf < mI2SEOB ) {
    *left++ = *buf++ >> 14;
    *right++ = *buf++ >> 14;
  }
}

Heard &ListenI2S::crossCorrelate()
{
  int32_t mx = 0;
  int32_t my = 0;
  for (int ix = 0; ix < mAudioLen; ix++) {
    mx += mLeftAudio[ix];
    my += mRightAudio[ix];
  }
  mx /= mAudioLen;
  my /= mAudioLen;

  double sx = 0.0;
  double sy = 0.0;
  for (int ix = 0; ix < mAudioLen; ix++) {
    sx += (double)(mLeftAudio[ix] - mx) * (double)(mLeftAudio[ix] - mx);
    sy += (double)(mRightAudio[ix] - my) * (double)(mRightAudio[ix] - my);
  }
  double denom = sqrt(sx*sy);

  double sxy = 0.0;

  int zIx = 0;
  int j = 0;
  double maxS = 0.0;
  int maxSIx = 0;
  for (int16_t delay = -mMaxDelay; delay < mMaxDelay; delay++) {
    sxy = 0.0;
    for (int16_t ix = 0; ix < mAudioLen; ix++) {
      j = ix + delay;
      if (j < 0 || j >= mAudioLen) {
        continue;
      }
      sxy += (mLeftAudio[ix] - mx) * (mRightAudio[j] - my);
    }
    mXCorr[zIx] = sxy / denom;
    if (mXCorr[zIx] > maxS) {
      maxS = mXCorr[zIx];
      maxSIx = zIx;
    }
    zIx++;
  }
  this->heard.direction = (2.0f * maxSIx / mXCorrLen) - 1.0f;
  this->heard.loudness = (int) denom / 1000000;
  this->heard.peak = maxS;
  //if (maxS <= 0.8 || denom < 2000000) {
  //  maxSIx = -1;
  //} else {
  if (maxS > 0.8f) {
    Serial.print("loud ");
    Serial.print(this->heard.loudness);
    Serial.print(" peak ");
    Serial.print(this->heard.peak);
    Serial.print(" direction ");
    Serial.println(this->heard.direction);
  }
  return this->heard;
}
