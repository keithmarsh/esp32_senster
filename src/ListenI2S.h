#pragma once

#include "Arduino.h"
#include "Heard.h"

class ListenI2S
{
public:
  ListenI2S(int bck, int ws, int dataIn, int i2sBuffLen, int maxDelay);
  ~ListenI2S();
  Heard &listen();
protected:
  const int mI2SBufferLen;
  uint8_t *mI2SBuffer;
  uint8_t *mI2SEOB;
  const int mAudioLen;
  int32_t *mLeftAudio, *mRightAudio;
  const int mMaxDelay;
  const int mXCorrLen;
  double *mXCorr;
  Heard heard;
protected:
  void captureI2S();
  Heard &crossCorrelate();
};
