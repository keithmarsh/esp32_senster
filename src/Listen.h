#pragma once

#include "Arduino.h"

class Listen
{
public:
  Listen(int left, int right, int sync);
  void tick();
protected:
  static const unsigned int SAMPLE_LEN = 200;
  static const unsigned int CROSS_COUNT = 5;
  static const unsigned int AVG_COUNT = 10;
  
  static const int MID;
  static const unsigned int THRESHOLD;
  static const unsigned int LOOK_BACK;
  static const unsigned int LOOK_FWD;
  static const float MIN_SLOPE;
  static const float MAX_IPD_USEC;

  // Sample data and timestamp in micro sec
  short left[SAMPLE_LEN];
  short right[SAMPLE_LEN];
  unsigned long time[SAMPLE_LEN];
  
  // timestamps of rising crossing of x-axis
  float lcross[CROSS_COUNT];
  float rcross[CROSS_COUNT];
  
  // last n differences between left and right crossings
  float ipd[AVG_COUNT];
  
  // holds the index for the next crossing trimestamps in idp
  unsigned int gIpdIx;
  short mMin;
  short mMax;
  
  int mLeftPin;
  int mRightPin;
  int mSyncPin;
protected:
  void captureStereo();
  int cleanStereoData();
  void detectRisingCrossing();
  void printStereoCSV();
  float calcAverageIPD();
  void printIPDs();
};
