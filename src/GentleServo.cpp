#include "GentleServo.h"

#define LOG (0)

//void printError(DynamixelStatus stat);

GentleServo::GentleServo(int ID, int maxSpeed, int accel, HardwareSerial &serial)
  : mMaxSpeed ( maxSpeed )
  , mAccel ( accel )
  , mCurrSpeed ( 0 )
  , mGoalSpeed ( 0 )
  , mMSecMoving ( 0 )
  , mStartPos ( 0 )
  , mGoalPos ( 0 )
  , mAX12 ( serial, ID )
{
  if ( mAccel == 0 ) {
    mMSecToTarget = 0;
    mAccelDistance = 0;
  } else {
    mMSecToTarget = mMaxSpeed * 1000L / mAccel;
    mAccelDistance = 2 * mAccel * mMSecToTarget * mMSecToTarget / 2000000L;
  }
#if LOG
  Serial.print(ID);
  Serial.print(" GentleServo::GentleServo accel ");
  Serial.print(mAccel);
  Serial.print(" mSecToTargetSpeed ");
  Serial.print(mMSecToTarget);
  Serial.print(" accelDistance ");
  Serial.println(mAccelDistance);
#endif
  bool isOK = mAX12.init();
  if (!isOK) {
    Serial.print(mAX12.id());
    Serial.println(" init failed");
  }
}

void GentleServo::start(long baud) {
  Serial.println("init");
}

bool GentleServo::isMoving() {
  //while(Serial1.available()) { Serial1.read(); }
  int isMoving = 0;
  bool isOK = mAX12.isMoving ( isMoving );
  int sanity = 10;
  while (!(isOK && isMoving <= 1) && --sanity > 0) {
    delay(10);
    Serial.print(" *** Bad isMoving ");
    Serial.println(isMoving);
    isOK = mAX12.isMoving ( isMoving );
  }
  if (sanity == 0) {
    isMoving = 0;
    Serial.println("*** isMoving insane!");
  }
  return isMoving != 0;
}

int GentleServo::getCurrentPosition() {
  int currPos;
  bool isOK = mAX12.getPosition(currPos);
  int sanity = 10;
  while (!(isOK && currPos <= 1023) && --sanity > 0) {
    Serial.print(" *** Bad getCurrentPosition ");
    Serial.println(currPos);
    delay(10);
    isOK = mAX12.getPosition(currPos);
  }
  if (sanity == 0) {
    currPos = 512;
    Serial.println("*** getCurrentPosition insane!");
  }
  return currPos;
}

void GentleServo::setTarget(int pos) {
  if ( mAccel == 0 ) {
    mCurrSpeed = mMaxSpeed;
  } else {
    mCurrSpeed = sStartSpeed;
  }
  mGoalPos = pos;

  mStartPos = getCurrentPosition();

  mGoalSpeed = mMaxSpeed;
  mMSecMoving = 0;
  mAX12.setSpeed(mCurrSpeed);

  bool isOK = mAX12.setPosition ( mGoalPos );

}

void GentleServo::tick(int mSecElapsed) {

  if ( mCurrSpeed == 0 || mAccel == 0 ) {
    return;
  }
  mMSecMoving += mSecElapsed;
  int32_t currPos = getCurrentPosition();
  bool isMoving = this->isMoving();
  if ( ! isMoving ) {
#if LOG
    Serial.print("STOP @ ");
    Serial.print(currPos);
    Serial.print(" after ");
    Serial.print(mMSecMoving);
    Serial.println(" mSec");
#endif
    mCurrSpeed = 0;
    return;
  }
  if ( ( mStartPos < mGoalPos ) && (mCurrSpeed == mGoalSpeed) && ( mGoalPos - currPos < mAccelDistance ) ) {
#if LOG
    Serial.println("decelerate CW!");
#endif
    mGoalSpeed = sStartSpeed;
    mMSecMoving  = mSecElapsed;
  } else if ( ( mStartPos > mGoalPos ) && (mCurrSpeed == mGoalSpeed) && ( currPos - mGoalPos < mAccelDistance ) ) {
#if LOG
    Serial.print("decelerate CCW startPos ");
    Serial.print(mStartPos);
    Serial.print(" mGoalPos ");
    Serial.print(mGoalPos);
    Serial.print(" currPos ");
    Serial.println(currPos);
#endif
    mGoalSpeed = sStartSpeed;
    mMSecMoving  = mSecElapsed;
  } else if (mCurrSpeed < mGoalSpeed && (abs(currPos - mStartPos) << 1 ) > abs(mGoalPos - mStartPos)) {
    // halfway and still accelerating
#if LOG
    Serial.print("halfway decelerate distance ");
    Serial.print(abs(mGoalPos - mStartPos));
    Serial.print(" to go ");
    Serial.println(abs(currPos - mStartPos));
#endif
    mGoalSpeed = sStartSpeed;
    mMSecMoving  = mMSecToTarget - mMSecMoving;
  } else if ( currPos == mGoalPos ) {
#if LOG
    Serial.print ("HOME ");
    Serial.println ( mMSecMoving );
#endif
  } else {
    //Serial.println ( currPos );
  }
  if (mCurrSpeed < mGoalSpeed) {
    // accelerating
    mCurrSpeed = min ( sStartSpeed + (mMSecMoving * mMaxSpeed / mMSecToTarget), mMaxSpeed );
    mAX12.setSpeed ( mCurrSpeed );
#if LOG
    Serial.print("tick currSpeed ");
    Serial.print(mCurrSpeed);
    Serial.print(" goalSpeed ");
    Serial.print(mGoalSpeed);
    Serial.print(" moving for ");
    Serial.print(mMSecMoving);
    Serial.print(" pos ");
    Serial.println(currPos);
#endif
  } else if (mCurrSpeed > mGoalSpeed) {
    mCurrSpeed = max ( mMaxSpeed - (mMSecMoving * mMaxSpeed / mMSecToTarget), sStartSpeed );
    mAX12.setSpeed ( mCurrSpeed );
#if LOG
    Serial.print("---- currSpeed ");
    Serial.print(mCurrSpeed);
    Serial.print(" goalSpeed ");
    Serial.print(mGoalSpeed);
    Serial.print(" moving for ");
    Serial.print(mMSecMoving);
    Serial.print(" pos ");
    Serial.println(currPos);
#endif
   } else if ( mGoalSpeed > 0 && mCurrSpeed >= mGoalSpeed ) {
    //Serial.print("maxSpeed at ");
    //Serial.println((mStartPos - currPos));
   } else if ( mGoalSpeed == 0 && mCurrSpeed <= mGoalSpeed ) {
    // reached stop
   } else {
    //idle or at maxspeed
  }
}

bool GentleServo::setIDBeCareful(int ID)
{
  return mAX12.setAddr(3,ID);
}
